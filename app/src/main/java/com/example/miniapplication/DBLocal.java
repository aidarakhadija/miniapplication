package com.example.miniapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.provider.ContactsContract;

import com.example.miniapplication.ui.Produit;
import com.example.miniapplication.ui.Produitdao;
import com.example.miniapplication.ui.User;
import com.example.miniapplication.ui.Userdao;


@Database(entities = {User.class,Produit.class},version = 2,exportSchema = false)
public abstract class DBLocal extends RoomDatabase{
    private  static  DBLocal dbLocal;
    private  static String db_name="db_local.db";
    public  synchronized  static  DBLocal getInstance(Context context){
        if(dbLocal==null){
            dbLocal= Room.databaseBuilder(context.getApplicationContext(),DBLocal.class,db_name)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return  dbLocal;
    }
        public abstract Userdao userdao();
    public abstract Produitdao produitdao();


}

