package com.example.miniapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.miniapplication.ui.Produit;
import com.example.miniapplication.ui.User;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private EditText txtLogin, txtPassword;
    private Button btnConnect;
    private String login, password;

    private DBLocal dbLocal ;
    public static User user ;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtLogin = findViewById(R.id.txtLogin);
        txtPassword = findViewById(R.id.txtPassword);

        btnConnect = findViewById(R.id.btnConnect);
        dbLocal =DBLocal.getInstance(getApplicationContext());
        dbLocal.produitdao().deletell();
        Produit produit=new Produit();
        produit.setNom("saovon");
        produit.setPrix("10");
        produit.setLieu("elton");
        produit.setDate("10/09/2020");
        dbLocal.produitdao().insert(produit);

        produit.setNom("lait");
        produit.setPrix("108");
        produit.setLieu("edk");
        produit.setDate("23/05/2020");
        dbLocal.produitdao().insert(produit);

        produit.setNom("sucre");
        produit.setPrix("1000");
        produit.setLieu("casino");
        produit.setDate("01/05/2020");
        dbLocal.produitdao().insert(produit);

        produit.setNom("pain");
        produit.setPrix("200");
        produit.setLieu("edk");
        produit.setDate("29/08/2020");
        dbLocal.produitdao().insert(produit);

        produit.setNom("riz");
        produit.setPrix("12000");
        produit.setLieu("senmarche");
        produit.setDate("18/04/2020");
        dbLocal.produitdao().insert(produit);
        produit.setNom("huile");
        produit.setPrix("10");
        produit.setLieu("auchan");
        produit.setDate("10/08/2020");
        dbLocal.produitdao().insert(produit);
        produit.setNom("chocolat");
        produit.setPrix("1000");
        produit.setLieu("casino");
        produit.setDate("17/07/2020");
        dbLocal.produitdao().insert(produit);
        produit.setNom("cacao");
        produit.setPrix("1000000");
        produit.setLieu("edk");
        produit.setDate("03/04/2020");
        dbLocal.produitdao().insert(produit);
        produit.setNom("fromage");
        produit.setPrix("3000");
        produit.setLieu("edk");
        produit.setDate("30/07/2020");
        dbLocal.produitdao().insert(produit);
        produit.setNom("jambon");
        produit.setPrix("367");
        produit.setLieu("edk");
        produit.setDate("10/09/2020");
        dbLocal.produitdao().insert(produit);
        dbLocal.userdao().deletell();
        user=new User();
        user.setLogin("tester");
        user.setPassword("passer");
        dbLocal.userdao().insert(user);


        List<Produit> list = dbLocal.produitdao().getAll();

        btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login = txtLogin.getText().toString().trim();
                password = txtPassword.getText().toString().trim();

                if(login.isEmpty() || password.isEmpty())
                {
                    Toast.makeText(MainActivity.this, "Veuillez renseigner les Champs", Toast.LENGTH_SHORT).show();
                }
                else{

                 User usercon=dbLocal.userdao().findByName(login,password);

                    if(usercon==null)
                    {
                        Toast.makeText(MainActivity.this, "Vos paramétres sont incorrects", Toast.LENGTH_SHORT).show();

                    }
                    else{
                        Intent intent = new Intent(MainActivity.this, navigation_view.class);
                        startActivity(intent);
                    }
                    }
            }
        });
    }
}
