package com.example.miniapplication.ui;

import android.icu.text.Replaceable;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy.*;
import androidx.room.Query;

import java.util.List;

@Dao
public interface Userdao {
    @Insert
    void insert(User user);
    @Delete
    void delete(User user);
    @Query("SELECT * FROM user")
    List<User>getAll();
    @Query("SELECT * FROM user WHERE login LIKE :login AND " +
            "password LIKE :password LIMIT 1")
    User findByName(String login ,String password);
    @Query("delete  FROM user where 1=1")
    void deletell();
}
