package com.example.miniapplication.ui;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;
@Dao
public interface Produitdao {
    @Insert
    void insert(Produit  produit);
    @Delete
    void delete(Produit produit);
    @Query("SELECT * FROM produit")
    List<Produit> getAll();
    @Query("delete  FROM produit")
    void deletell();

}
